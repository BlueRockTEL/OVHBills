<?php

# OVH API PARAMETERS:
define('APP_ENDPOINT', 'ovh-eu');
define('APP_KEY', '');
define('APP_SECRET', '');
define('APP_CONSKEY', '');

# DB PARAMETERS:
define('DB_SERVER', '');
define('DB_PORT', '');
define('DB_NAME', '');
define('DB_USERNAME', '');
define('DB_PASSWORD', '');
define('DB_CHARSET', '');

# PATHS:
define('ABSPATH', dirname(__FILE__));
define('BILLS_PATH', 'bills');

# THE DATE FROM WHICH WE GET THE BILLS:
define('START', '1 year ago');
#define('START', '1 month ago');
#define('START', '1 week ago');
#define('START', '3 days ago');
#define('START', '2 days ago');
#define('START', '1 day ago');

?>
