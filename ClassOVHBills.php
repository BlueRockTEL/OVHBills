<?php

# MIT License
#
# Copyright (c) 2017 Cyrille Georgel < cyrille@bluerocktel.com >
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

include_once 'config.php';

include_once 'pdo.php';

require __DIR__ . '/vendor/autoload.php';

use \Ovh\Api;


Class OVHBills extends Api
{

    /**
     * Create the tables `bills` and `billsDetails`
     * if they don't exist in the current database
     */
    private function createTables()
    {
        global $dbh;

        $sth = $dbh->prepare("CREATE TABLE IF NOT EXISTS `bills` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `billId` varchar(255) NOT NULL DEFAULT '',
          `nic` varchar(255) NOT NULL DEFAULT '',
          `billDate` datetime NOT NULL,
          `password` varchar(255) NOT NULL DEFAULT '',
          `orderId` varchar(255) NOT NULL DEFAULT '',
          `url` varchar(255) NOT NULL DEFAULT '',
          `currency` varchar(255) NOT NULL DEFAULT '',
          `priceWithoutTax` decimal(10,4) NOT NULL,
          `tax` decimal(10,4) NOT NULL,
          `priceWithTax` decimal(10,4) NOT NULL,
          `pdfUrl` varchar(255) NOT NULL DEFAULT '',
          `done` tinyint(4) NOT NULL DEFAULT '0',
          `outConsumption` decimal(10,4) NOT NULL,
          `outConsumptionSet` tinyint(4) NOT NULL DEFAULT '0',
          `deposit` decimal(10,4) NOT NULL,
          `depositSet` tinyint(4) NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`))");

        $sth->execute();

        $sth = $dbh->prepare("CREATE TABLE IF NOT EXISTS `billsDetails` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `billId` varchar(255) NOT NULL DEFAULT '',
          `billDetailId` varchar(255) NOT NULL DEFAULT '',
          `description` varchar(255) NOT NULL DEFAULT '',
          `domain` varchar(255) NOT NULL DEFAULT '',
          `periodStart` date DEFAULT NULL,
          `periodEnd` date DEFAULT NULL,
          `currency` varchar(255) NOT NULL DEFAULT '',
          `quantity` int(11) NOT NULL,
          `unitPrice` decimal(10,4) NOT NULL,
          `totalPrice` decimal(10,4) NOT NULL,
          PRIMARY KEY (`id`))");

        $sth->execute();
    }


    /**
     * @return array with the bills in the period
     */
    private function billsList()
    {
        $end = new DateTime();
        $start = new DateTime(START);

        $bills = $this->get('/me/bill?date.from=' . $start->format('Y-m-d')
        . '&date.to=' . $end->format('Y-m-d'));

        return $bills;
    }


    /**
     * Download the bill as a PDF file and provide feedback about the process
     * @param integer $bill
     * @return StdClass object
     */
    private function billGetFile($bill)
    {
        $result = new StdClass();

        $path = ABSPATH . '/' . BILLS_PATH;

        if(!file_exists ($path))
        {
            mkdir($path);
        }

        $billId = $this->get('/me/bill/' . $bill)['billId'];
        $pdfUrl = $this->get('/me/bill/' . $bill)['pdfUrl'];

        $localFile = BILLS_PATH . '/' . $billId . '.pdf';

        if(!file_exists($localFile))
        {
            $pdf = file_get_contents($pdfUrl);

            if(!file_put_contents($localFile, $pdf))
            {
                $result->status = 'ko';
                $result->feedback = 'Bill ' . $bill . 'download: error';
            }
            else
            {
                $result->status = 'ok';
                $result->feedback = 'Bill ' . $bill . ' download: success';
            }
        }
        else
        {
            $result->status = 'ko';
            $result->feedback = 'Bill ' . $bill . ' was already downloaded';
        }

        return $result;
    }


    /**
     * Get the details of the bill into tables `bills` and `billsDetails`
     * and provide some feedback about the process
     * @param integer $bill
     * @return StdClass object $result
     */
    private function billGetDetails($bill)
    {
        global $dbh;

        $result = new StdClass();

        $sth = $dbh->prepare("SELECT id
        FROM bills
        WHERE billId = ?");

        $sth->execute(array(
            $bill
        ));

        if($sth->rowCount() == 0)
        {
            $control = 0;

            $sth = $dbh->prepare("INSERT INTO bills
                (billId, nic, billDate, password, orderId, url,
                currency, priceWithoutTax, tax, priceWithTax, pdfUrl)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $document = $this->get('/me/bill/' . $bill);

            $sth->execute(array(
                $document['billId'],
                $this->get('/me')['nichandle'],
                $document['date'],
                $document['password'],
                $document['orderId'],
                $document['url'],
                $document['priceWithoutTax']['currencyCode'],
                $document['priceWithoutTax']['value'],
                $document['tax']['value'],
                $document['priceWithTax']['value'],
                $document['pdfUrl']
            ));

            $control++;

            if($sth->rowCount() == 1)
            {
                $control--;
            }


            $sth = $dbh->prepare("INSERT INTO billsDetails
                (billId, billDetailId, description, domain,
                periodStart, periodEnd, currency, quantity, unitPrice, totalPrice)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            $lines = $this->get('/me/bill/' . $bill . '/details');

            $control = count($lines);

            foreach($lines as $line)
            {
                $detail = $this->get('/me/bill/' . $bill . '/details/' . $line);

                $sth->execute(array(
                    $bill,
                    $detail['billDetailId'],
                    $detail['description'],
                    $detail['domain'],
                    $detail['periodStart'],
                    $detail['periodEnd'],
                    $document['priceWithoutTax']['currencyCode'],
                    $detail['quantity'],
                    $detail['unitPrice']['value'],
                    $detail['totalPrice']['value']
                ));

                if($sth->rowCount() == 1)
                {
                    $control--;
                }
            }

            if($control == 0)
            {
                $result->status = 'ok';
                $result->feedback = 'Bill ' . $bill . ' insertion in database: success';
            }
            else
            {
                $result->status = 'ko';
                $result->feedback = 'Bill ' . $bill . ' insertion in database: error';
            }
        }
        else
        {
            $result->status = 'ko';
            $result->feedback = 'Bill ' . $bill . ' was already in database';
        }

        return $result;
    }


    /**
     * Set the amount of `deposit` in the bill
     * and provide some feedback about the process
     * @param integer $bill
     * @return StdClass object $result
     */
    private function depositSet($bill)
    {
        /*
        Set the amount of "deposit" in the bill
        */

        global $dbh;

        # reinit deposit for bill:

        $sth = $dbh->prepare("UPDATE bills
        SET deposit = ?, depositSet = ?
        WHERE billId = ?");

        $sth->execute(array(
            0,
            0,
            $bill
        ));

        $result = new StdClass();

        $sth = $dbh->prepare("SELECT SUM(totalPrice) as total
        FROM billsDetails
        WHERE billId = ? AND description LIKE ?
        OR billId = ? AND description LIKE ?");

        $sth->execute(array(
            $bill,
            'Caution%',
            $bill,
            'Remboursement caution%'
        ));

        $data = $sth->fetchObject();

        $sth1 = $dbh->prepare("UPDATE bills
        SET deposit = ?, depositSet = ?
        WHERE billId = ?");

        $sth1->execute(array(
            $data->total,
            '1',
            $bill
        ));

        if($sth1->rowCount() == 1)
        {
            $result->status = 'ok';
            $result->feedback = 'Deposit has been added to bill ' . $bill;
        }
        else
        {
            $result->status = 'ko';
            $result->feedback = 'Error when trying to add deposit to bill ' . $bill;
        }

        return $result;
    }


    /**
     * Set the amount of `outConsumption` in the bill
     * and provide some feedback about the process
     * @param integer $bill
     * @return StdClass object $result
     */
    private function outConsumptionSet($bill)
    {
        /*
        Set the amount of "outConsumption" in the bill
        */

        global $dbh;

        # reinit outConsumption for bill:

        $sth = $dbh->prepare("UPDATE bills
        SET outConsumption = ?, outConsumptionSet = ?
        WHERE billId = ?");

        $sth->execute(array(
            0,
            0,
            $bill
        ));

        $result = new StdClass();

        $sth = $dbh->prepare("SELECT SUM(totalPrice) as total
        FROM billsDetails
        WHERE billId = ? AND description = ?");

        $sth->execute(array(
            $bill,
            'Communications non comprises dans vos forfaits'
        ));

        $data = $sth->fetchObject();

        $sth1 = $dbh->prepare("UPDATE bills
        SET outConsumption = ?, outConsumptionSet = ?
        WHERE billId = ?");

        $sth1->execute(array(
            $data->total,
            '1',
            $bill
        ));

        if($sth1->rowCount() == 1)
        {
            $result->status = 'ok';
            $result->feedback = 'outConsumption has been added to bill ' . $bill;
        }
        else
        {
            $result->status = 'ko';
            $result->feedback = 'Error when trying to add outConsumption to bill ' . $bill;
        }

        return $result;
    }


    /**
     * Put all the previous functions together, run the process...
     */
    public function run()
    {
        $tables = $this->createTables();

        $bills = $this->billsList();

        foreach($bills as $bill)
        {
            $download = $this->billGetFile($bill);

            print strtoupper($download->status) . ': ' . $download->feedback . PHP_EOL;

            $record = $this->billGetDetails($bill);

            print strtoupper($record->status) . ': ' . $record->feedback . PHP_EOL;

            $depositSet = $this->depositSet($bill);

            print strtoupper($depositSet->status) . ': ' . $depositSet->feedback . PHP_EOL;

            $outConsumptionSet = $this->outConsumptionSet($bill);

            print strtoupper($outConsumptionSet->status) . ': ' . $outConsumptionSet->feedback . PHP_EOL;

            print PHP_EOL;
        }
    }
}

# Usage:

$bills = new OVHBills(APP_KEY, APP_SECRET, APP_ENDPOINT, APP_CONSKEY);
$bills->run();

?>
